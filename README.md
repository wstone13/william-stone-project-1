## Project Description

The Expense Reimbursement Project is a browser-based application designed to allow user to update and manage expense reports.

## Technologies Used

* Core Java
* PostgreSQL 
* JavaScript 
* JDBC
* Servlets
* HTML
* CSS

## Features

List of features ready and TODOs for future development
* Login functionality
* Viewing of expense tickets
* Table filtering
* Unique options based on user type
* Creating new entries in database
* Updating ticket information

To-do list:
* Clean up JavaScript to do more DOM Manipulation
* Allow for table filtering to be available on the same page instead of needing to return to the home screen following filtering
* Add option to update tickets while viewing tickets
* Allow user to amend/delete a pending ticket