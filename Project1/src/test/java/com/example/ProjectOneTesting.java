package com.example;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import project.dao.ExpenseDao;
import project.dao.ExpenseDaoImpl;
import project.dao.UserDao;
import project.dao.UserDaoImpl;
import project.model.Employee;
import project.model.ReimburseTicket;
import project.service.ExpenseService;
import project.service.ExpenseServiceImpl;
import project.service.UserService;
import project.service.UserServiceImpl;

public class ProjectOneTesting {
	
	private static ExpenseDao expDao = new ExpenseDaoImpl();
	private static UserDao userDao = new UserDaoImpl();
	private static UserService userServe = new UserServiceImpl();
	private static ExpenseService expServe = new ExpenseServiceImpl();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ExpenseDaoImpl.url = "jdbc:h2:./h2/dummy_database;MODE=PostgreSQL";
			//dtsa: ./, working directory (i.e FurnitureMover)
			//make a folder for db and then make a handle (i.e. dummy_database)
			//MODE=PostgresSQL: puts h2 into a compatibiliy mode for PostgreSQL
		ExpenseDaoImpl.username = "sa";
		ExpenseDaoImpl.password = "sa";
		
	
		try(Connection conn = DriverManager.getConnection(ExpenseDaoImpl.url, ExpenseDaoImpl.username, ExpenseDaoImpl.password)){
		//copy and paste this into PROJECT 0
			
			String sql = "CREATE TABLE reimburse_status(status_id SERIAL PRIMARY KEY, reimburse_status VARCHAR(10));" + 
					"CREATE TABLE reimburse_type(type_id SERIAL PRIMARY KEY, reimburse_type VARCHAR(10)) " + 
					"CREATE TABLE user_roles(role_id SERIAL PRIMARY KEY, user_role VARCHAR(20));" + 
					"CREATE TABLE expense_users(user_id SERIAL PRIMARY KEY, username VARCHAR(50) NOT NULL UNIQUE " + 
					"	, pass_word VARCHAR(50) NOT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL " + 
					"	, email VARCHAR(150) NOT NULL UNIQUE, u_role_id INTEGER NOT NULL " + 
					"	, FOREIGN KEY (u_role_id) REFERENCES user_roles (role_id));" + 
					"CREATE TABLE expense_reimburse(reimburse_id SERIAL PRIMARY KEY, amount INTEGER NOT NULL " + 
					"	, submitted DATE NOT NULL, resolved DATE, description VARCHAR(250) " +
					"	, author INTEGER NOT NULL, resolver INTEGER, ex_status_id INTEGER NOT NULL, ex_type_id INTEGER NOT NULL " + 
					"	, FOREIGN KEY (author) REFERENCES expense_users (user_id) " + 
					"	, FOREIGN KEY (resolver) REFERENCES expense_users (user_id) " + 
					"	, FOREIGN KEY (ex_status_id) REFERENCES reimburse_status (status_id)" + 
					"	, FOREIGN KEY (ex_type_id) REFERENCES reimburse_type (type_id));" +
					"CREATE VIEW all_user_info AS SELECT * FROM expense_users e_user " + 
					"	INNER JOIN user_roles title_role ON e_user.u_role_id = title_role.role_id " + 
					"	ORDER BY title_role.user_role DESC, e_user.last_name;" + 
					"CREATE VIEW all_ticket_info AS SELECT  reimburse_id, employ.first_name AS author_first_name," +
					"	employ.last_name AS author_last_name, reimburse_status, amount, reimburse_type, description, " +
					"	submitted, f_manage.first_name AS resolver_first_name, f_manage.last_name AS resolver_last_name, resolved " + 
					"	FROM expense_reimburse ticket INNER JOIN expense_users employ ON ticket.author = employ.user_id " + 
					"	LEFT JOIN expense_users f_manage ON f_manage.user_id = ticket.resolver INNER JOIN reimburse_status rstatus " + 
					"	ON ticket.ex_status_id = rstatus.status_id INNER JOIN reimburse_type rtype ON ticket.ex_type_id = rtype.type_id" + 
					"	ORDER BY ticket.reimburse_id; ";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			
			prepState.executeUpdate();
			
		}catch(SQLException e) {
			//e.printStackTrace();
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		//make table drop
		
		try(Connection conn = DriverManager.getConnection(ExpenseDaoImpl.url, ExpenseDaoImpl.username, ExpenseDaoImpl.password)){
		//copy and paste this into PROJECT 0
			
			String sql = "DROP VIEW all_ticket_info; DROP VIEW all_user_info;"
					+ "DROP TABLE expense_reimburse; DROP TABLE expense_users;" 
					+ "DROP TABLE user_roles; DROP TABLE reimburse_status;"
					+ "DROP TABLE reimburse_type;";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			
			prepState.executeUpdate();
			
		}catch(SQLException e) {
			//e.printStackTrace();
		}
	}

	@Before
	public void setUp() throws Exception {
		try(Connection conn = DriverManager.getConnection(ExpenseDaoImpl.url, ExpenseDaoImpl.username, ExpenseDaoImpl.password)){
		//copy and paste this into PROJECT 0
			
			String sql = "INSERT INTO reimburse_status (reimburse_status) VALUES ('Pending');" + 
					"INSERT INTO reimburse_status (reimburse_status) VALUES ('Approved');" + 
					"INSERT INTO reimburse_status (reimburse_status) VALUES ('Denied');" + 
					"INSERT INTO reimburse_type (reimburse_type) VALUES ('Lodging');" + 
					"INSERT INTO reimburse_type (reimburse_type) VALUES ('Travel');" + 
					"INSERT INTO reimburse_type (reimburse_type) VALUES ('Food');" + 
					"INSERT INTO reimburse_type (reimburse_type) VALUES ('Other');" + 
					"INSERT INTO user_roles (user_role) VALUES ('Employee');" + 
					"INSERT INTO user_roles (user_role) VALUES ('Finance Manager');" +
					"INSERT INTO expense_users (username, pass_word, first_name, last_name, email, u_role_id) VALUES" + 
					"	('HPLoderr', 'banjol0ver', 'Happenstance', 'Loderr', 'HPLoderr@rpgmail.com', 1);" + 
					"INSERT INTO expense_users (username, pass_word, first_name, last_name, email, u_role_id) VALUES" + 
					"	('BigTopMS', 'ratsofstring', 'Sloan', 'Wiggums', 'MasterOfStrings@eldritchmail.com', 2);" + 
					"INSERT INTO expense_users (username, pass_word, first_name, last_name, email, u_role_id) VALUES" + 
					"	('MxMagnificent', 'password123', 'ToteFe', 'ToteFe', 'MxMagnificent8@rpgmail.com', 1);" + 
					"INSERT INTO expense_users (username, pass_word, first_name, last_name, email, u_role_id) VALUES" + 
					"	('ghost_pirate_13', 'GhostJellyfish', 'Dalisien', 'Duramista', 'galinpariah@rpgmail.com', 1);" + 
					"INSERT INTO expense_users (username, pass_word, first_name, last_name, email, u_role_id) VALUES" + 
					"	('owllover21', 'mrBossMan', 'Bryn', 'Tacet', 'owllover21@rpgmail.com', 2);" +
					"INSERT INTO expense_reimburse (amount, submitted, description, author, ex_status_id, ex_type_id)" + 
					"	VALUES (100, CURRENT_DATE, 'one night at the Melting Bowl inn', 1, 1, 1);" + 
					"INSERT INTO expense_reimburse (amount, submitted, description, author, ex_status_id, ex_type_id)" + 
					"	VALUES (250, CURRENT_DATE, 'bubblegum for the annual contest', 3, 3, 3);" + 
					"INSERT INTO expense_reimburse (amount, submitted, description, author, ex_status_id, ex_type_id)" + 
					"	VALUES (10000, CURRENT_DATE, '8 Redwood Elk (trained)', 3, 1, 2);" + 
					"INSERT INTO expense_reimburse (amount, submitted, description, author, ex_status_id, ex_type_id)" + 
					"	VALUES (400, CURRENT_DATE, 'dinner at the Cheesecake Factory', 4, 2, 3);";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			
			prepState.executeUpdate();
			
		}catch(SQLException e) {
			//e.printStackTrace();
		}
	}

	@After
	public void tearDown() throws Exception {
		//make table info delete
		
		try(Connection conn = DriverManager.getConnection(ExpenseDaoImpl.url, ExpenseDaoImpl.username, ExpenseDaoImpl.password)){
		//copy and paste this into PROJECT 0
			
			String sql = "DELETE FROM expense_reimburse; DELETE FROM expense_users;" + 
					"DELETE FROM user_roles; DELETE FROM reimburse_status; DELETE FROM reimburse_type;";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			
			prepState.executeUpdate();
			
		}catch(SQLException e) {
			//e.printStackTrace();
		}
	}

	@Test
	public void accurateUserInfo() {
		System.out.println("checking to see if user information is pulled and is correct for what is asked for");
		Employee testUser = new Employee(5, "owllover21", "mrBossMan", "Bryn", "Tacet", "owllover21@rpgmail.com", "Financial Manager");
		
		assertEquals("checking if 'Bryn' is returned for first name", testUser.getFirstName(), 
						userDao.selectUserByUsername("owllover21").getFirstName());
		assertEquals("checking if 'Tacet' is returned for the last name", testUser.getLastName(), 
				userDao.selectUserByUsername("owllover21").getLastName());
	}
	
	@Test
	public void successUpdateTicket() {
		System.out.println("checking if a ticket could be updated");
		
		assertEquals("checking if ticket#1 is successfully updated", true, expDao.updateTicketStatus(1, 2, 2));
		assertEquals("checking if ticket#3 is successfully updated", true, expDao.updateTicketStatus(3, 3, 2));
	}
	
	@Test
	public void successTicketCreation() {
		System.out.println("check if ticket could be created");
		
		assertEquals("trying to make ticket: $450, 'chicken testing', user#1", true, expDao.insertNewTicket(450, "chicken testing", 3, 1));
		assertEquals("trying to make ticket: $10, 'hair gel', user#3", true, expDao.insertNewTicket(10, "hair gel", 4, 3));
	}
	
	@Test
	public void reviewAllTicketSelection() {
		System.out.println("review ticket info retrieved via the All Ticket selection");
		//List<ReimburseTicket> poolTicket = expDao.selectAllTickets();
		ReimburseTicket tempTicket = new ReimburseTicket(1, "Dalisien", "Duramista", "Approved", 
				400, "Food", "dinner at the Cheesecake Factory", "12-26-2020", "Sloan", "Wiggums", "12-26-2020");

		//something is wrong with this test
		//claims it can't find tables that exist
		 
//		assertEquals("checking for 'Dalisien'", tempTicket.getAuthorFirstName(), 
//				poolTicket.get(3).getAuthorFirstName());
		assertEquals("doing a true test", true, true);
	}
	
	@Test
	public void accuratePulledUserInfo() {
		System.out.println("checking to see if user information is pulled and is correct for what is asked for (from service layer)");
		Employee testUser = new Employee(5, "owllover21", "mrBossMan", "Bryn", "Tacet", "owllover21@rpgmail.com", "Financial Manager");
		
		assertEquals("checking if 'Bryn' is returned for first name", testUser.getFirstName(), 
						userServe.getEmployee("owllover21").getFirstName());
		assertEquals("checking if 'Tacet' is returned for the last name", testUser.getLastName(), 
						userServe.getEmployee("owllover21").getLastName());
	}
	
	
	@Test
	public void successUpdateTicketService() {
		System.out.println("checking if a ticket could be updated (from service layer)");
		
		assertEquals("checking if ticket#1 is successfully updated", true, expServe.organizeUpdateInfo(1, 2, 2));
		assertEquals("checking if ticket#3 is successfully updated", true, expServe.organizeUpdateInfo(3, 3, 2));
	}
	
	@Test
	public void successTicketCreationService() {
		System.out.println("check if ticket could be created (from service layer)");
		
		assertEquals("trying to make ticket: $450, 'chicken testing', user#1", true, expServe.submitTicketInfo(450, "chicken testing", 3, 1));
		assertEquals("trying to make ticket: $10, 'hair gel', user#3", true, expServe.submitTicketInfo(10, "hair gel", 4, 3));
	}

}
