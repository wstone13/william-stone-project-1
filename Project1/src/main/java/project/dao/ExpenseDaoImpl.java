package project.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import project.MainDriver;
import project.model.ReimburseTicket;

public class ExpenseDaoImpl implements ExpenseDao{
	
	public final static Logger loggy = Logger.getLogger(MainDriver.class);
	
	public static String url = "jdbc:postgresql://paracosm.cl45s6nyu5la.us-west-2.rds.amazonaws.com/reinbursement";
	public static String username= "Paracosm";
	public static String password= "p4ssw0rd";

	static { 
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
    }

	
	/**
	 * This collects all reimbursement tickets present in the database,
	 * regardless of status
	 * 
	 * @return List
	 */
	@Override
	public List<ReimburseTicket> selectAllTickets() { //DONE
		List<ReimburseTicket> ticketList = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM all_ticket_info";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			
			ResultSet rSet = prepState.executeQuery(); //<--query not update
			
			while(rSet.next()) {
				ticketList.add(new ReimburseTicket(	rSet.getInt(1), rSet.getString(2),
						rSet.getString(3), rSet.getString(4), rSet.getInt(5),
						rSet.getString(6), rSet.getString(7), rSet.getString(8),
						rSet.getString(9), rSet.getString(10), rSet.getString(11)));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		loggy.info("pooled all tickets into a list");
		return ticketList;
	}
	
	
	/**
	 * This creates a new reimbursement ticket.
	 * The data comes from an employee.
	 * It will leave the author and resolution time empty.
	 * 
	 * @param amount, summary, ex_type_id, author
	 */
	@Override
	public boolean insertNewTicket(int amount, String summary, int ex_type_id, int author) { //DONE
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "INSERT INTO expense_reimburse (amount, submitted, description, author, ex_status_id, ex_type_id) " + 
								"VALUES (?, CURRENT_DATE, ?, ?, 1, ?)";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			prepState.setInt(1, amount);
			prepState.setString(2, summary);
			prepState.setInt(3, author);
			prepState.setInt(4, ex_type_id);
			
			prepState.executeQuery();
			
		}catch(SQLException e) {
			//e.printStackTrace();
		}
		
		loggy.info("created a new ticket");
		return true;
	}

	
	/**
	 * This creates and returns a list of all tickets in the database
	 * based on the employee who created it
	 * 
	 * @param firstName, lastName
	 * @return List
	 */
	@Override
	public List<ReimburseTicket> selectAllTicketsByUserName(String firstName, String lastName) { //DONE
		List<ReimburseTicket> ticketList = new ArrayList<>();
		
//		try(Connection conn = DriverManager.getConnection(MyConnectionHub.url, MyConnectionHub.username,
//			MyConnectionHub.password)){
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM all_ticket_info WHERE author_first_name = ? AND author_last_name = ?";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			prepState.setString(1, firstName);
			prepState.setString(2, lastName);
			
			ResultSet rSet = prepState.executeQuery(); //<--query not update
			
			while(rSet.next()) {
				ticketList.add(new ReimburseTicket(	rSet.getInt(1), rSet.getString(2),
						rSet.getString(3), rSet.getString(4), rSet.getInt(5),
						rSet.getString(6), rSet.getString(7), rSet.getString(8),
						rSet.getString(9), rSet.getString(10), rSet.getString(11)));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		loggy.info("pooled all tickets made by a specifc user into a list");
		return ticketList;
	}

	
	/**
	 * This creates and returns a list of all tickets in the database
	 * based on its status
	 * 
	 * @param status
	 * @return List
	 */
	@Override
	public List<ReimburseTicket> selectTicketsByStatus(String status) { //DONE
		List<ReimburseTicket> ticketList = new ArrayList<>();
		
//		try(Connection conn = DriverManager.getConnection(MyConnectionHub.url, MyConnectionHub.username,
//			MyConnectionHub.password)){
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM all_ticket_info WHERE reimburse_status = ?";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			prepState.setString(1, status);
			
			ResultSet rSet = prepState.executeQuery(); //<--query not update
			
			while(rSet.next()) {
				ticketList.add(new ReimburseTicket(	rSet.getInt(1), rSet.getString(2),
						rSet.getString(3), rSet.getString(4), rSet.getInt(5),
						rSet.getString(6), rSet.getString(7), rSet.getString(8),
						rSet.getString(9), rSet.getString(10), rSet.getString(11)));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		loggy.info("pooled all tickets into a list of a specific status");
		return ticketList;
	}

	
	/**
	 * This creates and returns a list of all tickets in the database
	 * made by a specific employee that has a specific status.
	 * 
	 * @param firstName, lastName, status
	 * @return List
	 */
	@Override
	public List<ReimburseTicket> selectTicketsByStatusByUserID(String firstName, String lastName, String status) {
		List<ReimburseTicket> ticketList = new ArrayList<>();
		
//		try(Connection conn = DriverManager.getConnection(MyConnectionHub.url, MyConnectionHub.username,
//			MyConnectionHub.password)){
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM all_ticket_info WHERE author_first_name = ? AND author_last_name = ? AND reimburse_status = ?;";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			prepState.setString(1, firstName);
			prepState.setString(2, lastName);
			prepState.setString(3, status);
			
			ResultSet rSet = prepState.executeQuery(); //<--query not update
			
			while(rSet.next()) {
				ticketList.add(new ReimburseTicket(	rSet.getInt(1), rSet.getString(2),
						rSet.getString(3), rSet.getString(4), rSet.getInt(5),
						rSet.getString(6), rSet.getString(7), rSet.getString(8),
						rSet.getString(9), rSet.getString(10), rSet.getString(11)));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		loggy.info("pooled all tickets made by a user into a list based on status");
		return ticketList;
	}

	
	/**
	 * This updates the status in a specific ticket and adds data
	 * for the resolver and the time it was resolved
	 * 
	 * @param ticketID, statusID, resolverID
	 */
	@Override
	public boolean updateTicketStatus(int ticketID, int statusID, int resolverID) { //DONE
		
		
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "UPDATE expense_reimburse SET ex_status_id = ?, resolver = ?, resolved = current_date WHERE reimburse_id = ?;";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			prepState.setInt(1, statusID);
			prepState.setInt(2, resolverID);
			prepState.setInt(3, ticketID);

			prepState.executeQuery();
			
		}catch(SQLException e) {
			//e.printStackTrace();
		}
		
		loggy.info("update a ticket's status");
		return true;
		
	}
	

}
