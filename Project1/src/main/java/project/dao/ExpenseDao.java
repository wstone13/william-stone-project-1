package project.dao;

import java.util.List;

import project.model.ReimburseTicket;

public interface ExpenseDao {

	//SELECTS
	public List<ReimburseTicket> selectAllTickets();
	public List<ReimburseTicket> selectAllTicketsByUserName(String firstName, String lastName);
	public List<ReimburseTicket> selectTicketsByStatus(String status);
	public List<ReimburseTicket> selectTicketsByStatusByUserID(String firstName, String lastName, String status);
	
	//CREATE
	public boolean insertNewTicket(int amount, String summary, int ex_type_id, int author);
	
	//UPDATE
	public boolean updateTicketStatus(int ticketID, int statusID, int resolverID);
	
}
