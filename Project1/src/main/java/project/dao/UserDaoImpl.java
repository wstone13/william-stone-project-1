package project.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import project.MainDriver;
import project.model.Employee;

public class UserDaoImpl implements UserDao {
	
	
	public final static Logger loggy = Logger.getLogger(MainDriver.class);
	
	public static String url = "jdbc:postgresql://paracosm.cl45s6nyu5la.us-west-2.rds.amazonaws.com/reinbursement";
	public static String username= "Paracosm";
	public static String password= "p4ssw0rd";

	static { 
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
    }
	
	/**
	 * This creates an Employee object by pulling data 
	 * based on a username
	 * 
	 * @param use_username
	 * @return Employee
	 */
	@Override
	public Employee selectUserByUsername(String use_username) { //DONE
		
		Employee pullUser = new Employee();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM all_user_info WHERE username = ?";
			
			PreparedStatement prepState = conn.prepareStatement(sql);
			prepState.setString(1, use_username);
			
			ResultSet rSet = prepState.executeQuery(); //<--query not update
			
			if(!rSet.next()) {
				return null;
			}
			
				pullUser = new Employee(rSet.getInt(1), rSet.getString(2),
						rSet.getString(3), rSet.getString(4), rSet.getString(5), 
						rSet.getString(6), rSet.getString(9));
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		loggy.info("collected a user based on their username");
		return pullUser;
		
		
	}
	
	

}
