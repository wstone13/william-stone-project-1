package project.dao;

import project.model.Employee;

public interface UserDao {
	
	public Employee selectUserByUsername(String username);

}
