package project.service;

import java.util.List;

import org.apache.log4j.Logger;

import project.MainDriver;
import project.dao.ExpenseDao;
import project.dao.ExpenseDaoImpl;
import project.model.ReimburseTicket;

public class ExpenseServiceImpl implements ExpenseService {
	
	public final static Logger loggy = Logger.getLogger(MainDriver.class);
	public static ExpenseDao expDao = new ExpenseDaoImpl();
	
	
	/**
	 * Forms a list of all tickets pulled in and passes it up
	 * 
	 * @return List
	 */
	@Override
	public List<ReimburseTicket> getAllTickets() { //DONE
		//creating a list of available homes in the database
		List<ReimburseTicket> myList = expDao.selectAllTickets();
		loggy.info("forms list of all ticket entries");
		
		//passing it back to showcase the data or use it
		return myList;
	}
	
	
	/**
	 * Forms a list of all tickets for a specific employee 
	 * and passes it up
	 * 
	 * @param firstName, lastName
	 * @return List
	 */
	@Override
	public List<ReimburseTicket> getUserTickets(String firstName, String lastName) { //DONE
		loggy.info("forms list of all ticket entries for a user");
		return expDao.selectAllTicketsByUserName(firstName, lastName);
	}


	/**
	 * Forms a list of all tickets of a specific status
	 * and passes it up
	 * 
	 * @param statusId
	 * @return List
	 */
	@Override
	public List<ReimburseTicket> getStatusTickets(int statusId) { //DONE
		if(statusId == 1) {
			loggy.info("forms list of pending ticket entries");
			return expDao.selectTicketsByStatus("Pending");
		}else if(statusId == 2) {
			loggy.info("forms list of approved ticket entries");
			return expDao.selectTicketsByStatus("Approved");
		}else {
			loggy.info("forms list of denied ticket entries");
			return expDao.selectTicketsByStatus("Denied");
		}
	}
	
	
	/**
	 * Forms a list of all tickets of a specific status written
	 * by a specific employee and passes it up
	 * 
	 * @param firstName, lastName, statusId
	 * @return List
	 */
	@Override
	public List<ReimburseTicket> getUserTicketsByStatus(String firstName, String lastName, int statusId) { //DONE
		if(statusId == 1) {
			loggy.info("forms list of pending ticket entries for a user");
			return expDao.selectTicketsByStatusByUserID(firstName, lastName, "Pending");
		}else if(statusId == 2) {
			loggy.info("forms list of approved ticket entries for a user");
			return expDao.selectTicketsByStatusByUserID(firstName, lastName, "Approved");
		}else {
			loggy.info("forms list of denied ticket entries for a user");
			return expDao.selectTicketsByStatusByUserID(firstName, lastName, "Denied");
		}
	}

	
	/**
	 * Pass info to the dao to create a new ticket entry into the database
	 * 
	 * @param amount, summary, ex_type_id, author
	 */
	@Override
	public boolean submitTicketInfo(int amount, String summary, int ex_type_id, int author) { //DONE
		loggy.info("passes ticket into dao for ticket creation");
		expDao.insertNewTicket(amount, summary, ex_type_id, author);
		return true;
	}


	/**
	 * Pass ingo into the dao to update a ticket's information in the database
	 * 
	 * @param ticketId, statusId, resolverId
	 */
	@Override
	public boolean organizeUpdateInfo(int ticketId, int statusId, int resolverId) { //DONE
		loggy.info("passes ticket into dao for ticket updating");
		expDao.updateTicketStatus(ticketId, statusId, resolverId);
		return true;
	}

}

