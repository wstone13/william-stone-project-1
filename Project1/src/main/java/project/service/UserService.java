package project.service;

import project.model.Employee;

public interface UserService {
	
	public Employee getEmployee(String username);

}

