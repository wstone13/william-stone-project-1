package project.service;

import org.apache.log4j.Logger;

import project.MainDriver;
import project.dao.UserDao;
import project.dao.UserDaoImpl;
import project.model.Employee;

public class UserServiceImpl implements UserService {
	
	public final static Logger loggy = Logger.getLogger(MainDriver.class);
	public static UserDao useDao = new UserDaoImpl();

	
	/**
	 * Assigns the Employee data from the dao to its own Employee
	 * variable and passes it up
	 * 
	 * @param username
	 * @return Employee
	 */
	@Override
	public Employee getEmployee(String username) {
		loggy.info("pulling user info in service layer");
		Employee pullUser = useDao.selectUserByUsername(username);
		return pullUser;
	}

}
