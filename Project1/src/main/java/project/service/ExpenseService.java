package project.service;

import java.util.List;

import project.model.ReimburseTicket;

public interface ExpenseService {
	
	public List<ReimburseTicket> getAllTickets();
	public List<ReimburseTicket> getUserTickets(String firstName, String lastName);
	public List<ReimburseTicket> getStatusTickets(int statusId);
	public List<ReimburseTicket> getUserTicketsByStatus(String firstName, String lastName, int statusId);

	public boolean submitTicketInfo(int amount, String summary, int ex_type_id, int author);
	
	public boolean organizeUpdateInfo(int ticketId, int statusId, int resolverId);

}

