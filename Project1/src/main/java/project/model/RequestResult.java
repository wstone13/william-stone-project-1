package project.model;

import java.util.Arrays;

public class RequestResult {
	
	
	private boolean success; //tells client is ajax was successful
	private String failureMessage; //tells client what went wrong
	private Object[] data; //where you'd store a collection of reimbursements
	
	public RequestResult(){}

	public RequestResult(boolean success, String failureMessage, Object[] data) {
		super();
		this.success = success;
		this.failureMessage = failureMessage;
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getFailureMessage() {
		return failureMessage;
	}

	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}

	public Object[] getData() {
		return data;
	}

	@Override
	public String toString() {
		return "RequestResult [success=" + success + ", failureMessage=" + failureMessage + ", data="
				+ Arrays.toString(data) + "]";
	}

	public void setData(Object[] data) {
		this.data = data;
	}
	
	

}

