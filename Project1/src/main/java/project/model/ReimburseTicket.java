package project.model;

public class ReimburseTicket {
	
	private int ticketId;
	private String authorFirstName;
	private String authorLastName;
	private String ticketStatus;
	private int amount;
	private String ticketType;
	private String summary;
	private String submitTime;
	private String resolverFirstName;
	private String resolverLastName;
	private String resolveTime;
	
	ReimburseTicket(){
		
	}

	public ReimburseTicket(int ticketId, String authorFirstName, String authorLastName, String ticketStatus, int amount,
			String ticketType, String summary, String submitTime, String resolverFirstName, String resolverLastName, 
			String resolveTime) {
		super();
		this.ticketId = ticketId;
		this.authorFirstName = authorFirstName;
		this.authorLastName = authorLastName;
		this.ticketStatus = ticketStatus;
		this.amount = amount;
		this.ticketType = ticketType;
		this.summary = summary;
		this.submitTime = submitTime;
		this.resolverFirstName = resolverFirstName;
		this.resolverLastName = resolverLastName;
		this.resolveTime = resolveTime;
	}

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public String getAuthorFirstName() {
		return authorFirstName;
	}

	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}

	public String getAuthorLastName() {
		return authorLastName;
	}

	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}

	public String getResolverFirstName() {
		return resolverFirstName;
	}

	public void setResolverFirstName(String resolverFirstName) {
		this.resolverFirstName = resolverFirstName;
	}

	public String getResolverLastName() {
		return resolverLastName;
	}

	public void setResolverLastName(String resolverLastName) {
		this.resolverLastName = resolverLastName;
	}

	public String getResolveTime() {
		return resolveTime;
	}

	public void setResolveTime(String resolveTime) {
		this.resolveTime = resolveTime;
	}
	
	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	@Override
	public String toString() {
		return "ReimburseTicket [ticketId=" + ticketId + ", authorFirstName=" + authorFirstName + ", authorLastName="
				+ authorLastName + ", ticketStatus=" + ticketStatus + ", amount=" + amount + ", ticketType=" + ticketType 
				+ ", summary=" + summary + ", submitTime=" + submitTime + ", resolverFirstName=" + resolverFirstName 
				+ ", resolverLastName=" + resolverLastName + ", resolveTime=" + resolveTime + "]";
	}

}

