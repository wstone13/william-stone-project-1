package project.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import project.MainDriver;
import project.controller.HomeController;
import project.controller.TicketController;

public class ForwardRequestHelper {
	
	public final static Logger loggy = Logger.getLogger(MainDriver.class);
	
	public static String process(HttpServletRequest request, HttpServletResponse response) {
		
		response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.addHeader("Pragma", "no-cache");
		response.addIntHeader("Expires", 0);

		//System.out.println(request.getRequestURI());
		
		switch(request.getRequestURI()) {
		
		//login
		case "/Project1/forwarding/goHome":
			loggy.info("inside project 1 go home case");
			return HomeController.home(request);
			
		//financial manager views
		case "/Project1/forwarding/viewTickets":
			loggy.info("trying to view all tickets");
			return HomeController.view(request);
		case "/Project1/forwarding/viewPendingTickets":
			loggy.info("trying to view pending tickets");
			return HomeController.viewPending(request);
		case "/Project1/forwarding/viewApprovedTickets":
			loggy.info("trying to view approved tickets");
			return HomeController.viewApprove(request);
		case "/Project1/forwarding/viewDeclinedTickets":
			loggy.info("trying to view declined tickets");
			return HomeController.viewDecline(request);
			
		//employee views
		case "/Project1/forwarding/viewTicketsUser":
			loggy.info("trying to view all tickets");
			return HomeController.viewUser(request);
		case "/Project1/forwarding/viewPendingTicketsUser":
			loggy.info("trying to view pending tickets");
			return HomeController.viewPendingUser(request);
		case "/Project1/forwarding/viewApprovedTicketsUser":
			loggy.info("trying to view approved tickets");
			return HomeController.viewApproveUser(request);
		case "/Project1/forwarding/viewDeclinedTicketsUser":
			loggy.info("trying to view declined tickets");
			return HomeController.viewDeclineUser(request);

		//ticket creation	
		case "/Project1/forwarding/createTicket":
			loggy.info("going to create ticket page");
			return HomeController.makeTicket(request);
			
		//ticket update	
		case "/Project1/forwarding/updateTicket":
			loggy.info("going to update ticket page");
			return HomeController.updateTicket(request);
			
		//logout
		case "/Project1/forwarding/returnLogin":
			loggy.info("logging out");
			loggy.info("------------------------");
			return HomeController.returnLogin(request);
		default:
				return "/resources/html/user_badlogin.html";
		}

	}


}

