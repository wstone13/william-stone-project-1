package project.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ForwardingMasterServlet
 */
public class ForwardingMasterServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.addHeader("Pragma", "no-cache");
		response.addIntHeader("Expires", 0);
		
		request.getRequestDispatcher(ForwardRequestHelper.process(request, response)).forward(request, response);
			//System.out.println("ForwardingMaster, doGet");
			//System.out.println(request.getRequestURI());
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.addHeader("Pragma", "no-cache");
		response.addIntHeader("Expires", 0);
		
		request.getRequestDispatcher(ForwardRequestHelper.process(request, response)).forward(request, response);
//			System.out.println("ForwardingMaster, doPost");
//			System.out.println(request.getRequestURI());
	}

}
