package project.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import project.MainDriver;
import project.controller.HomeController;
import project.controller.LoginController;
import project.controller.TicketController;

public class AjaxRequestHelper {
	
	public final static Logger loggy = Logger.getLogger(MainDriver.class);

	public static void process(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.addHeader("Pragma", "no-cache");
		response.addIntHeader("Expires", 0);
		
		//System.out.println(request.getRequestURI());

		switch (request.getRequestURI()) {
		
		//ticket creation
		case "/Project1/ajax/makeTicket":
			loggy.info("going to create ticket page");
			TicketController.makeTicket(request, response);
			break;
			
		//ticket update
		case "/Project1/ajax/updateTicket":
			loggy.info("going to update ticket page");
			TicketController.updateTicket(request, response);
			break;
		
		//finance manager views
		case "/Project1/ajax/allTickets":
			loggy.info("going to view all ticket page");
			TicketController.allFinder(request, response);
			break;
			
		case "/Project1/ajax/pendingTickets":
			loggy.info("going to view pending ticket page");
			TicketController.pendFinder(request, response);
			break;
			
		case "/Project1/ajax/approvedTickets":
			loggy.info("going to view approved ticket page");
			TicketController.appFinder(request, response);
			break;
			
		case "/Project1/ajax/declinedTickets":
			loggy.info("going to view denied ticket page");
			TicketController.declFinder(request, response);
			break;
			
		//employee views
		case "/Project1/ajax/allTicketsUser":
			loggy.info("going to view all ticket by user page");
			TicketController.allFinderUser(request, response);
			break;
			
		case "/Project1/ajax/pendingTicketsUser":
			loggy.info("going to view pending ticket by user page");
			TicketController.pendFinderUser(request, response);
			break;
			
		case "/Project1/ajax/approvedTicketsUser":
			loggy.info("going to view approved ticket by user page");
			TicketController.appFinderUser(request, response);
			break;
			
		case "/Project1/ajax/declinedTicketsUser":
			loggy.info("going to view declined ticket by user page");
			TicketController.declFinderUser(request, response);
			break;
			
		//login handling	
		case "/Project1/ajax/loginHandle":
			loggy.info("------------------------");
			loggy.info("logging in");
			LoginController.login(request, response);
			break;
			
		default:
			response.getWriter().println("null");
		}
	}


}

