package project;

import java.util.List;

import org.apache.log4j.Logger;

import project.model.ReimburseTicket;
import project.service.ExpenseService;
import project.service.ExpenseServiceImpl;
import project.service.UserService;
import project.service.UserServiceImpl;

public class MainDriver {
	
	public final static Logger loggy = Logger.getLogger(MainDriver.class);
	static ExpenseService expServe = new ExpenseServiceImpl();
	static UserService useServe = new UserServiceImpl();
	
	public static void main(String[] args) {
		List<ReimburseTicket> ticketList = expServe.getAllTickets();
		for(ReimburseTicket iterate : ticketList)
			System.out.println(iterate);
		
		List<ReimburseTicket> ticketUserList = expServe.getAllTickets();
		System.out.println("Tickets for User by Status: ");
		for(ReimburseTicket iterate : ticketUserList)
			System.out.println(iterate);
		
//		Scanner scan = new Scanner(System.in);
//		System.out.print("Enter ticket id: ");
//		int ticketId = scan.nextInt();
//		System.out.print("Enter status id: ");
//		int statusId = scan.nextInt();
//		System.out.print("Enter resolver id: ");
//		int resolverId = scan.nextInt();
		
//		expServe.organizeUpdateInfo(ticketId, statusId, resolverId);
		
		System.out.println("goodbye!");
		loggy.info("ending program");
		loggy.info("-------------------------");
	}

}
