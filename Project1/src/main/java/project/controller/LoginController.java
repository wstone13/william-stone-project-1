package project.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import project.MainDriver;
import project.model.Employee;
import project.model.RequestResult;
import project.service.UserService;
import project.service.UserServiceImpl;

public class LoginController {
	
	static UserService useServe = new UserServiceImpl();
	public final static Logger loggy = Logger.getLogger(MainDriver.class);
	
	
	/**
	 * This collects the info passed from the client. It checks if the request was POST,
	 * if the username exists in the system, and if the password matches the one for the
	 * desired username. If all three are sufficient, then it creates a session for that
	 * user.
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public static void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		PrintWriter writer = response.getWriter();
		
		if(!request.getMethod().contentEquals("POST")) {
			//This is what happens if the request sent here
			//wasn't using a POST method
			//aka if the data isn't secure, go back to the
			//login page

			writer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "Wrong HTTP Method", null)));
				//this is a safety object for the FETCH to return so that it doesn't return null
			
			loggy.info("Wrong HTTP Method");
			return ; //returns void, makes the rest of the code not execute (aka a method break)
		}
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		
		Employee pullUser = useServe.getEmployee(username);
		
		//check if user exists
		if(pullUser == null) {
			writer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "This user doesn't exist", null)));
			loggy.info("This user doesn't exist");
			return ;
		}
		
		//check if password is right
		if(!password.equals(pullUser.getPassword())) {
			writer.write(new ObjectMapper().writeValueAsString(new RequestResult(false, "This isn't the right password for this account", null)));
			loggy.info("This isn't the right password for this account");
			return ;
		}
		
		//make session for a specific user
		request.getSession().setAttribute("session_user", pullUser);
		Employee setUser = (Employee) request.getSession().getAttribute("session_user");
		loggy.info("------------------------");
		loggy.info("session created for: " + setUser.getUsername());
		
		writer.write(new ObjectMapper().writeValueAsString(new RequestResult(true, "Successful login", null)));	
		
	}

}

