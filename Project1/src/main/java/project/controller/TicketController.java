package project.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import project.MainDriver;
import project.model.Employee;
import project.model.ReimburseTicket;
import project.service.ExpenseService;
import project.service.ExpenseServiceImpl;

public class TicketController {

	public static ExpenseService expServe = new ExpenseServiceImpl();
	public final static Logger loggy = Logger.getLogger(MainDriver.class);
	
	
	//ticket creation
	/**
	 * Collects info from the session for ticket creation, converts it
	 * into a usable format, and the passes it to be created inside 
	 * of the database.
	 * 
	 * @param request
	 * @param response
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static void makeTicket(HttpServletRequest request, HttpServletResponse response)
			 throws JsonProcessingException, IOException{ //DONE

		loggy.info("parsing submitted information for ticket creation");
		
		Employee pullUser = (Employee) request.getSession().getAttribute("session_user");
		
		int amount = Integer.parseInt(request.getParameter("exp_amount"));
		String summary = request.getParameter("exp_description");
		int type = Integer.parseInt(request.getParameter("exp_type"));
		int author = pullUser.getUserId();
		
		//System.out.println("amount: "+amount+", summary :"+summary+", type: "+type);
		
		expServe.submitTicketInfo(amount, summary, type, author);
		
	}
	
	
	//ticket update
	/**
	 * Collects info from the session for ticket updating, converts it
	 * into a usable format, and the passes it to be created inside 
	 * of the database if the ticket id requested matches anything
	 * inside of it.
	 * 
	 * @param request
	 * @param response
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static void updateTicket(HttpServletRequest request, HttpServletResponse response)
			 throws JsonProcessingException, IOException{

		loggy.info("parsing submitted info for ticket updating");
		
		Employee pullUser = (Employee) request.getSession().getAttribute("session_user");
		
		int ticket = Integer.parseInt(request.getParameter("ticket_id"));
		int status = Integer.parseInt(request.getParameter("exp_status"));
		int resolver = pullUser.getUserId();
		
		List<ReimburseTicket> pendList = expServe.getStatusTickets(1);
		boolean ticketReal = false;
		
		for(ReimburseTicket checkList : pendList) {
			if(checkList.getTicketId() == ticket)
				ticketReal = true;
		}
		
		if(ticketReal) {
			loggy.info("This is a valid pending ticket");
			expServe.organizeUpdateInfo(ticket, status, resolver);
		}else {
			loggy.info("This is not a valid pending ticket");
		}
		
	}
	
	
	
	//finance manager views
	/**
	 * Pulling in info for all tickets and writing it into the
	 * session for presentation
	 * 
	 * @param request
	 * @param response
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static void allFinder(HttpServletRequest request, HttpServletResponse response)
			 throws JsonProcessingException, IOException{
		/*
		 * THIS IS WHERE YOU'D GO TO THE DATABASE TO GET THE OBJECTS TO SEND TO THE CLIENT
		 */

		List<ReimburseTicket> ticketList = expServe.getAllTickets();

		loggy.info("generate all ticket info in server");
		response.getWriter().write(new ObjectMapper().writeValueAsString(ticketList));
	}
	
	/**
	 * Pulling in info for pending tickets and writing it into the
	 * session for presentation
	 * 
	 * @param request
	 * @param response
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static void pendFinder(HttpServletRequest request, HttpServletResponse response)
			 throws JsonProcessingException, IOException{
		List<ReimburseTicket> ticketList = expServe.getStatusTickets(1);
		
		loggy.info("generate pending ticket info in server");
		response.getWriter().write(new ObjectMapper().writeValueAsString(ticketList));
	}
	
	/**
	 * Pulling in info for approved tickets and writing it into the
	 * session for presentation
	 * 
	 * @param request
	 * @param response
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static void appFinder(HttpServletRequest request, HttpServletResponse response)
			 throws JsonProcessingException, IOException{
		List<ReimburseTicket> ticketList = expServe.getStatusTickets(2);
		
		loggy.info("generate approved ticket info in server");
		response.getWriter().write(new ObjectMapper().writeValueAsString(ticketList));
	}
	
	/**
	 * Pulling in info for declined tickets and writing it into the
	 * session for presentation
	 * 
	 * @param request
	 * @param response
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static void declFinder(HttpServletRequest request, HttpServletResponse response)
			 throws JsonProcessingException, IOException{
		List<ReimburseTicket> ticketList = expServe.getStatusTickets(3);
		
		loggy.info("generate declined ticket info in server");
		response.getWriter().write(new ObjectMapper().writeValueAsString(ticketList));
	}
	
	
	
	//employee views
	/**
	 * Pulling in info for all tickets for a user and writing it into the
	 * session for presentation
	 * 
	 * @param request
	 * @param response
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static void allFinderUser(HttpServletRequest request, HttpServletResponse response)
			 throws JsonProcessingException, IOException{
		
		Employee pullUser = (Employee) request.getSession().getAttribute("session_user");
		String firstName = pullUser.getFirstName();
		String lastName = pullUser.getLastName();

		List<ReimburseTicket> ticketList = expServe.getUserTickets(firstName, lastName);
		
		loggy.info("generate all ticket info for a user in server");
		response.getWriter().write(new ObjectMapper().writeValueAsString(ticketList));
	}
	
	/**
	 * Pulling in info for pending tickets for a user and writing it into the
	 * session for presentation
	 * 
	 * @param request
	 * @param response
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static void pendFinderUser(HttpServletRequest request, HttpServletResponse response)
			 throws JsonProcessingException, IOException{
		Employee pullUser = (Employee) request.getSession().getAttribute("session_user");
		String firstName = pullUser.getFirstName();
		String lastName = pullUser.getLastName();
		
		List<ReimburseTicket> ticketList = expServe.getUserTicketsByStatus(firstName, lastName, 1);
		
		loggy.info("generate pending ticket info for a user in server");
		response.getWriter().write(new ObjectMapper().writeValueAsString(ticketList));
	}
	
	/**
	 * Pulling in info for approved tickets for a user and writing it into the
	 * session for presentation
	 * 
	 * @param request
	 * @param response
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static void appFinderUser(HttpServletRequest request, HttpServletResponse response)
			 throws JsonProcessingException, IOException{
		Employee pullUser = (Employee) request.getSession().getAttribute("session_user");
		String firstName = pullUser.getFirstName();
		String lastName = pullUser.getLastName();
		
		List<ReimburseTicket> ticketList = expServe.getUserTicketsByStatus(firstName, lastName, 2);
		
		loggy.info("generate approved ticket info for a user in server");
		response.getWriter().write(new ObjectMapper().writeValueAsString(ticketList));
	}
	
	/**
	 * Pulling in info for declined tickets for a user and writing it into the
	 * session for presentation
	 * 
	 * @param request
	 * @param response
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static void declFinderUser(HttpServletRequest request, HttpServletResponse response)
			 throws JsonProcessingException, IOException{
		Employee pullUser = (Employee) request.getSession().getAttribute("session_user");
		String firstName = pullUser.getFirstName();
		String lastName = pullUser.getLastName();
		
		List<ReimburseTicket> ticketList = expServe.getUserTicketsByStatus(firstName, lastName, 3);
		
		loggy.info("generate declined ticket info for a user in server");
		response.getWriter().write(new ObjectMapper().writeValueAsString(ticketList));
	}
	

}
