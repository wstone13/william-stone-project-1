package project.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import project.MainDriver;
import project.model.Employee;
import project.model.ReimburseTicket;
import project.service.ExpenseService;
import project.service.ExpenseServiceImpl;

public class HomeController{
	
	/*
	 * This communicates with the RequestHelper
	 * where to take the user when they request
	 * to go to the "user home page"
	 */
	
	public static ExpenseService expServe = new ExpenseServiceImpl();
	public final static Logger loggy = Logger.getLogger(MainDriver.class);
	
	
	/**
	 * This checks if the user for the session exists, and if so,
	 * directs them to the correct home page
	 * 
	 * @param request
	 * @return String
	 */
	public static String home(HttpServletRequest request) { //DONE
		
		//use this to access the session block info
		Employee pullUser = (Employee) request.getSession().getAttribute("session_user");
	
		if(pullUser == null) {
			return "/resources/html/login.html";
		}
		
		
		if(pullUser.getUserRole().equals("Employee")){
			loggy.info("entering employee interface");
			return "/resources/html/employee_home.html";
		} else {
			loggy.info("entering finance manager interface");
			return "/resources/html/f_manager_home.html";
		}
		
	}

	/**
	 * Directs to the "every ticket" table
	 * 
	 * @param request
	 * @return String
	 */
	public static String view(HttpServletRequest request) { //DONE
		loggy.info("viewing the 'every ticket' table");
		return "/resources/html/all_table.html";
	}
	
	/**
	 * Directs to the "pending tickets" table
	 * 
	 * @param request
	 * @return String
	 */
	public static String viewPending(HttpServletRequest request) {
		loggy.info("viewing the 'pending tickets' table");
		return "/resources/html/pending_table.html";
	}
	
	/**
	 * Directs to the "approved tickets" table
	 * 
	 * @param request
	 * @return String
	 */
	public static String viewApprove(HttpServletRequest request) {
		loggy.info("viewing the 'approved tickets' table");
		return "/resources/html/approved_table.html";
	}
	
	/**
	 * Directs to the "declined tickets" table
	 * 
	 * @param request
	 * @return String
	 */
	public static String viewDecline(HttpServletRequest request) {
		loggy.info("viewing the 'declined tickets' table");
		return "/resources/html/declined_table.html";
	}
	
	/**
	 * Directs to the "every ticket for a user" table
	 * 
	 * @param request
	 * @return String
	 */
	public static String viewUser(HttpServletRequest request) {
		loggy.info("viewing the 'every ticket for a user' table");
		return "/resources/html/user_all_table.html";
	}
	
	/**
	 * Directs to the "pending tickets for a user" table
	 * 
	 * @param request
	 * @return String
	 */
	public static String viewPendingUser(HttpServletRequest request) {
		loggy.info("viewing the 'pending tickets for a user' table");
		return "/resources/html/user_pending_table.html";
	}
	
	/**
	 * Directs to the "approved tickets for a user" table
	 * 
	 * @param request
	 * @return String
	 */
	public static String viewApproveUser(HttpServletRequest request) {
		loggy.info("viewing the 'approved tickets for a user' table");
		return "/resources/html/user_approved_table.html";
	}
	
	/**
	 * Directs to the "declined tickets for a user" table
	 * 
	 * @param request
	 * @return String
	 */
	public static String viewDeclineUser(HttpServletRequest request) {
		loggy.info("viewing the 'declined tickets for a user' table");
		return "/resources/html/user_declined_table.html";
	}
	
	/**
	 * Directs to the "create a ticket" page
	 * 
	 * @param request
	 * @return String
	 */
	public static String makeTicket(HttpServletRequest request) {
		loggy.info("going to the 'create a ticket' page");
		return "/resources/html/create_ticket.html";
	}
	
	/**
	 * Directs to the "update a ticket" page
	 * 
	 * @param request
	 * @return String
	 */
	public static String updateTicket(HttpServletRequest request) {
		loggy.info("going to the 'update a ticket' page");
		return "/resources/html/update_ticket.html";
	}
	
	/**
	 * Directs to the "login" page
	 * 
	 * @param request
	 * @return String
	 */
	public static String returnLogin(HttpServletRequest request) {
		loggy.info("returning to home page");
		return "/login.html";
	}

}
