/**
 * 
 */

console.log("sup");

 window.onload = function() {

    //this page is waiting for the user to click on the "login button"
    document.getElementById("loginSubmit").addEventListener("click", async function(){
        const log_username = document.getElementById("loginUsername");
        const log_password = document.getElementById("loginPassword");

        let requested_info = await fetch('http://localhost:9006/Project1/ajax/loginHandle', 
            /*This is us requesting info from the server*/
            {
                method : "post",
                headers : { "Content-type": "application/x-www-form-urlencoded" },
                    /* this is how you send stuff WITHOUT JSON */
                body : `${log_username.name}=${log_username.value}&${log_password.name}=${log_password.value}`
                    /* turns "username" and "password" from html into a variable handle within the server */
            } )

        // console.log(`${ log_username.name } = ${ log_username.value }
        // & ${ log_password.name } = ${ log_password.value }`);
        
        //alert('promise object: ', response_object);
            
        /* this is a promise returning a JSON object, aka the info we requested from the server*/
        let response_obj = await requested_info.json().then( function(jsonCall){return jsonCall} );
					//this is the CustomErrorCheck, not the session
        

        if(response_obj.success == true){
        	//page won't 
        	//window.location.replace("http://localhost:9006/Project1/forwarding/goHome");   //caused loading issues
            window.location.assign("http://localhost:9006/Project1/forwarding/goHome");
        }

    });




 }
