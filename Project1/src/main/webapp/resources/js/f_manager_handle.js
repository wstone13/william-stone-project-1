console.log("in all_ticket_view");

window.onload = function() {

   //this page is waiting for the user to click on the "login button"
   document.getElementById("check_all_tickets").addEventListener("click", async function(){
        console.log("clicked the ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/viewTickets");
   });
   
   
   document.getElementById("update_tickets").addEventListener("click", async function(){
        console.log("clicked the update ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/updateTicket");
   });
   
   
   document.getElementById("check_pending_tickets").addEventListener("click", async function(){
        console.log("clicked the pending ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/viewPendingTickets");
   });
   
   
   document.getElementById("check_approved_tickets").addEventListener("click", async function(){
        console.log("clicked the complete ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/viewApprovedTickets");
   });
   
   
   document.getElementById("check_denied_tickets").addEventListener("click", async function(){
        console.log("clicked the complete ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/viewDeclinedTickets");
   });
   
   
   document.getElementById("return_login").addEventListener("click", async function(){
        console.log("clicked the logout button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/returnLogin");
   });
   
}
