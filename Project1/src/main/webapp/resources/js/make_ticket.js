console.log("inside make_ticket");

window.onload = function() {

    //this page is waiting for the user to click on the "login button"
    document.getElementById("ticketSubmit").addEventListener("click", async function(){
         console.log("clicked ticketSubmit");   
 
         const make_amount = document.getElementById("expenseAmount");
         const make_summary = document.getElementById("expenseDescription");
         const make_type = document.querySelector('input[name="exp_type"]:checked');

		////////////////////////////////////////////
		if(`${make_amount.name}=${make_amount.value}` == "exp_amount=" | `${make_amount.name}=${make_amount.value}` == "e"){
			alert("You need to input an amount for the reimbursement.");
		}else if (make_type == null){
			alert("You need to select an expense type.");
		}else{
	         let requested_info = await fetch('http://localhost:9006/Project1/ajax/makeTicket', 
	            /*This is us requesting info from the server*/
	            {
	                method : "post",
	                headers : { "Content-type": "application/x-www-form-urlencoded" },
	                    /* this is how you send stuff WITHOUT JSON */
	                body : `${make_amount.name}=${make_amount.value}&${make_summary.name}=${make_summary.value}&${make_type.name}=${make_type.value}`
	                    /* turns info from html into a variable handle within the server */
	            } )
	
			alert("Successful ticket creation!");
			window.location.href = ("http://localhost:9006/Project1/forwarding/goHome");
 		}
 
    });
    
    
	//this page is waiting for the user to click on the "login button"
   document.getElementById("return_home").addEventListener("click", async function(){
        console.log("clicked the make_ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/goHome");
   });
 
 }