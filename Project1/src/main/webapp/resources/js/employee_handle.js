console.log("in go_create_ticket");

window.onload = function() {

   //this page is waiting for the user to click on the "login button"
   document.getElementById("make_ticket").addEventListener("click", async function(){

        console.log("clicked the make_ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/createTicket");

   });
   
   
   document.getElementById("check_all_tickets").addEventListener("click", async function(){
        console.log("clicked the ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/viewTicketsUser");
   });
   
   
   document.getElementById("check_pending_tickets").addEventListener("click", async function(){
        console.log("clicked the pending ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/viewPendingTicketsUser");
   });
   
   
   document.getElementById("check_approved_tickets").addEventListener("click", async function(){
        console.log("clicked the complete ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/viewApprovedTicketsUser");
   });
   
   
   document.getElementById("check_denied_tickets").addEventListener("click", async function(){
        console.log("clicked the complete ticket button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/viewDeclinedTicketsUser");
   });
   
   document.getElementById("return_login").addEventListener("click", async function(){

        console.log("clicked the logout button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/returnLogin");

   });

}