/**
 * 
 */
 
 window.onload = function() {
	
	//I don't want to add an event listener because the page load IS THE EVENT
	//in your project 1, you'll be going to the server here to get the session data...because THAT is who
	// is currently logged in
	getAllExpTickets();
	
	          //this page is waiting for the user to click on the "login button"
   document.getElementById("return_home").addEventListener("click", async function(){

        console.log("clicked the return home button");    
        window.location.href = ("http://localhost:9006/Project1/forwarding/goHome");

   });
	
}

let myUserInformation ={};

function getAllExpTickets() {
	fetch(
			'http://localhost:9006/Project1/ajax/declinedTicketsUser')
			.then(function(daResponse) {
				const convertedResponse = daResponse.json();
				return convertedResponse;
			}).then(function(daSecondResponse) {
				console.log(daSecondResponse);
				myUserInformation=daSecondResponse;
				ourDOMManipulation(daSecondResponse);
			})

}


function ourDOMManipulation(ourJSON) {

	for (let i = 0; i < ourJSON.length; i++) {

		// ///////////CREATE ELEMENTS DYNAMICALLY//////////////
		// step1: creating our new element
		let newDiv = document.createElement("li");

		// step3: create a text node, then append to our new div element
		//let divText = document.createTextNode("ExpenseTicket: "+ourJSON[i].authorFirstName+", "+ourJSON[i].authorLastName);
		let divText = document.createTextNode("");
		newDiv.appendChild(divText);

		// step4: appending our new div element onto an existing element that is
		// currently being displayed
		//let newSelection = document.querySelector("#idTicketList");
		//newSelection.appendChild(newDiv);

		console.log(newDiv);
		///////////////table time
	    
	    if(ourJSON[i].resolverFirstName == null){
			ourJSON[i].resolverFirstName = "---";
			ourJSON[i].resolverLastName = "---";
			ourJSON[i].resolveTime = "---";
		}
	    
	    	 // ///////////CREATE ELEMENTS DYNAMICALLY//////////////
		// all creations
		let newTR = document.createElement("tr");
		let newTH = document.createElement("th");
		
		let newTD1 = document.createElement("td");
		let newTD2 = document.createElement("td");
		let newTD3 = document.createElement("td");
		let newTD4 = document.createElement("td");
		let newTD5 = document.createElement("td");
		let newTD6 = document.createElement("td");
		let newTD7 = document.createElement("td");
		let newTD8 = document.createElement("td");
		let newTD9 = document.createElement("td");
		let newTD10 = document.createElement("td");
		
		
		// population creations
		newTH.setAttribute("scope", "row")
		let myText1 = document.createTextNode(ourJSON[i].ticketId);
		let myText2 = document.createTextNode(ourJSON[i].authorFirstName);
		let myText3 = document.createTextNode(ourJSON[i].authorLastName);
		let myText4 = document.createTextNode(ourJSON[i].ticketStatus);
				let myText5 = document.createTextNode(ourJSON[i].amount);
		let myText6 = document.createTextNode(ourJSON[i].ticketType);
		let myText7 = document.createTextNode(ourJSON[i].summary);
		let myText8 = document.createTextNode(ourJSON[i].submitTime);
				let myText9 = document.createTextNode(ourJSON[i].resolverFirstName);
		let myText10 = document.createTextNode(ourJSON[i].resolverLastName);
		let myText11 = document.createTextNode(ourJSON[i].resolveTime);

		newDiv.appendChild(divText);
		
		
		
		///all appendings
		newTH.appendChild(myText1);
		newTD1.appendChild(myText2);
		newTD2.appendChild(myText3);
		newTD3.appendChild(myText4);
		newTD4.appendChild(myText5);
		newTD5.appendChild(myText6);
		newTD6.appendChild(myText7);
		newTD7.appendChild(myText8);
		newTD8.appendChild(myText9);
		newTD9.appendChild(myText10);
		newTD10.appendChild(myText11);
		
		newTR.appendChild(newTH);
		newTR.appendChild(newTD1);
		newTR.appendChild(newTD2);
		newTR.appendChild(newTD3);
		newTR.appendChild(newTD4);
		newTR.appendChild(newTD5);
		newTR.appendChild(newTD6);
		newTR.appendChild(newTD7);
		newTR.appendChild(newTD8);
		newTR.appendChild(newTD9);
		newTR.appendChild(newTD10);

		
		let newSelectionTwo = document.querySelector("#ticketTableBody");
		newSelectionTwo.appendChild(newTR);

		//additional note: consider pagination
		// my endpoint:
		//			website.net?startpagination=21&endpagination=40
		//			OR
		//			website.net?pagination=3
	    

	}
}
