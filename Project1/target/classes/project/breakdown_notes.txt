BREAKDOWN NOTES (Java files)


CONTROLLERS
	how you communicate with the html files

JSON (JavaScript Object Notation)
	how you communicate with the database (Postgres) and servlets
	a standardized dataformat similar to Object Literals	

MODEL
	objects for handling database info

SERVLET
	Java class that is tasked with handling a single URI in the server
	extended capability for the server
