PROJECT 1: Expense Reimbursement System (ERS)

Project Summary
	- manage process of reimbursing employees for expenses
		- not actually pushing money, just charting
	- this site is meant for a specific company
	- Employees
		- login
		- submit reimbursement requests
		- view past tickets
		- view pending requests
	- Finance Manager
		- login
		- view all reimbursement requests
		- view past history of all employees
		- approve/deny requests for reimbursement
	- Reimbursement Lifecycle
		- creation --> pending --> approved --> closed
				      \             /
					--> denied
	- Reimbursement Types
		- lodging
		- travel
		- food
		- other
